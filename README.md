# fortunateones.tk

The website of a Guild Wars 2 guild, Fortunate Ones [Fo].

This is a static website which is built with Jekyll and deployed with Netlify.

## Dependencies

* Ruby 2.3
* Jekyll 3.8
* Bootstrap 4
* jQuery 3

## Development and Production

Provided you have Ruby & RubyGems installed;

```bash
# Install Bundler
gem install bundler

# Navigate to project root folder
cd path/to/fortunateones/

# Install correct Jekyll version
bundle install

# Start development server
bundle exec jekyll serve

# Build for production
bundle exec jekyll build

# You can omit `bundle exec` from the commands
# but using it will make sure you execute them
# with the correct Jekyll version
```
